//
//  PetInfoViewController.swift
//  DoironNicholas_PetCare
//
//  Created by Nicholas Doiron on 8/16/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

import UIKit
import Firebase

class PetInfoViewController: UIViewController {
    
    @IBOutlet weak var petName: UITextField!
    @IBOutlet weak var petAge: UITextField!
    @IBOutlet weak var petBreed: UITextField!
    @IBOutlet weak var petType: UITextField!
    
    var ref: DatabaseReference!
    var count = 0
    var section = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addPet(_ sender: UIButton) {
        if petName.text != nil && petAge.text != nil && petType.text != nil && petBreed.text != nil{
            let user = Auth.auth().currentUser
            
            ref = Database.database().reference()
            
            count += 1
            
            ref?.child("pets").child((user?.uid)!).childByAutoId().setValue(["pet_name": petName.text!, "pet_breed": petBreed.text!, "pet_age": petAge.text!, "pet_type": petType.text!, "section" : section])
            
            performSegue(withIdentifier: "sendCount", sender: sender)
        }
        else{
            let alert = UIAlertController(title: "Error!", message: "One of the fields is not filled out", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! PetView
        
        destination.count = count
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
