//
//  PetView.swift
//  DoironNicholas_PetCare
//
//  Created by Nicholas Doiron on 8/10/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

import UIKit
import Firebase

class PetView: UIViewController, UITableViewDelegate, UITableViewDataSource
{

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var petTable: UITableView!
    
    var ref: DatabaseReference!
    
    var viewTitle = ""
    var count = 0
    var pets = [String]()
    var selectedPet = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let button = UIBarButtonItem(title: "Add Pet", style: .plain, target: self, action: #selector(addAnimal))
        
        self.navBar.topItem?.setRightBarButton(button, animated: true)
        self.navBar.topItem?.title = viewTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let current = Auth.auth().currentUser
        
        ref = Database.database().reference()
        
        ref.child("users").child((current?.uid)!).updateChildValues(["pet_count": count])
        
        ref.child("pets").child((current?.uid)!).observe(.value) { (snapshot) in
            for animals in snapshot.children{
                let animal = animals as! DataSnapshot
                let information = animal.value as! [String : Any]
                let piece = information["pet_name"] as! String
                let category = information["section"] as! String
                if category == self.viewTitle{
                    if !self.pets.contains(piece){
                        self.pets.append(piece)
                        self.count += 1
                    }
                }
            }
            self.petTable.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func addAnimal() {
        performSegue(withIdentifier: "toPetInfo", sender: nil)
        }
    
    @IBAction func returnToMenu(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func unwindTo(segue:UIStoryboardSegue) {
        let source = segue.source as! PetInfoViewController
        
        count = source.count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPetInfo"{
            let destination = segue.destination as! PetInfoViewController
            destination.section = (navBar.topItem?.title)!
        }else if segue.identifier == "petDisplay"{
            let destination = segue.destination as! PetDisplayViewController
            
            destination.selectedPet = selectedPet
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pets", for: indexPath)
        
        cell.textLabel?.text = pets[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPet = pets[indexPath.row]
        
        performSegue(withIdentifier: "petDisplay", sender: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
