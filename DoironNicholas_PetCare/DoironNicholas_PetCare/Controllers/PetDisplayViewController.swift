//
//  PetDisplayViewController.swift
//  DoironNicholas_PetCare
//
//  Created by Nicholas Doiron on 8/18/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class PetDisplayViewController: UIViewController {

    var ref: DatabaseReference?
    var petCare = ""

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var petInfo: UITextView!
    
    var selectedPet = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navBar.topItem?.title = selectedPet
        
        ref = Database.database().reference()
        
        ref?.child("pets").child((Auth.auth().currentUser?.uid)!).observe(.value, with: { (snapshot) in
            for animals in snapshot.children{
                let animal = animals as! DataSnapshot
                let animalInfo = animal.value as! [String : Any]
                if self.navBar.topItem?.title == animalInfo["pet_name"] as? String{
                    let age = animalInfo["pet_age"] as! String
                    let breed = animalInfo["pet_breed"] as! String
                    let type = animalInfo["pet_type"] as! String
                    let category = animalInfo["section"] as! String
                    if let path = Bundle.main.path(forResource: "document", ofType: ".json")
                    {
                        do {
                            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                            if let pet = jsonResult as? [String : Any]{
                                if let petBreed = pet[type.lowercased()] as? [String : Any]{
                                    if let petCareInfo = petBreed[breed.lowercased()] as? String{
                                        self.petInfo.text = "Age: \(age)\n\nBreed: \(breed)\n\nAnimal Type: \(type)\n\nAnimal Category: \(category)\n\n\(petCareInfo)"
                                    }
                                }else if let petBreed = pet[category.lowercased()] as? [String : Any]{
                                    if let petCareInfo = petBreed[type.lowercased()] as? String{
                                        print(petCareInfo)
                                        self.petInfo.text = "Age: \(age)\n\nBreed: \(breed)\n\nAnimal Type: \(type)\n\nAnimal Category: \(category)\n\n\(petCareInfo)"
                                    }
                                }
                            }
                        } catch {
                            var error = error.localizedDescription
                            print(error)
                            error = "No care information can be found on this animal"
                            self.petInfo.text = "Age: \(age)\n\nBreed: \(breed)\n\nAnimal Type: \(type)\n\nAnimal Category: \(category)\n\(error)"

                        }
                    }
                }
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissView(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
