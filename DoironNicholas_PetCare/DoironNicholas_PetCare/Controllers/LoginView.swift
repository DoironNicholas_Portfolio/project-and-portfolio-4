//
//  LoginView.swift
//  DoironNicholas_PetCare
//
//  Created by Nicholas Doiron on 8/11/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

//Imports
import UIKit
import FirebaseAuth
import FirebaseDatabase

//Reference to dataase
var ref: DatabaseReference?

class LoginView: UIViewController {

    //Outlets
    @IBOutlet weak var selectedOption: UISegmentedControl!
    @IBOutlet weak var repeatPassword: UITextField!
    @IBOutlet weak var repeatPasswordLabel: UILabel!
    @IBOutlet weak var signUpLoginBtn: UIButton!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        emailText.text = ""
        passwordText.text = ""
        repeatPassword.text = ""
        if selectedOption.selectedSegmentIndex == 1{
            signUpLoginBtn.titleLabel?.text = "Login"
        }else if selectedOption.selectedSegmentIndex == 0{
            signUpLoginBtn.titleLabel?.text = "Sign Up"
        }
    }
    
    //Function to hide objects on change
    @IBAction func changedOption(_ sender: Any) {
        //If sign up is selected
        if selectedOption.selectedSegmentIndex == 0{
            //Makes the secondary password appear
            repeatPassword.isHidden = false
            repeatPasswordLabel.isHidden = false
            signUpLoginBtn.titleLabel?.text = "Sign Up"
            //If login is selected
        }else if selectedOption.selectedSegmentIndex == 1{
            //Makes the secondary password disappear
            repeatPasswordLabel.isHidden = true
            repeatPassword.isHidden = true
            signUpLoginBtn.titleLabel?.text = "Log In"
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    //Function that signs up new users and logs them in
    @IBAction func performSignUpLoginAction(_ sender: Any) {
        //If sign up is selected
        if selectedOption.selectedSegmentIndex == 0 {
            //Checks to make sure that the fields aren't empty
            if emailText.text != "" || passwordText.text != "" || repeatPassword.text != ""{
                //Checks to see if the passwords match
                if passwordText.text == repeatPassword.text{
                    //Adds the user to the auth with email and password

                    Auth.auth().createUser(withEmail: emailText.text!, password: passwordText.text!) { (user, error) in
                        //Checks the user
                        if user != nil{
                            ref = Database.database().reference()
                            //Creates a child with the user id and then adds the information to that
                            ref?.child("users").child((user?.uid)!).setValue(["username": self.emailText.text!, "password": self.passwordText.text!, "pet_count": 0])
                            //Goes to the next view
                            self.performSegue(withIdentifier: "toMenu", sender: sender)
                            
                        }else{
                            //Presents the error the user
                            if let myError = error?.localizedDescription{
                                let alert = UIAlertController(title: "Error!", message: myError, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                self.present(alert, animated: true)
                            }
                            //User nil's else
                        }
                    }
                    //Password Check's else
                }else{
                    //Presents error to user
                    let alert = UIAlertController(title: "Error!", message: "Passwords do not match", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true)
                }

                //Empty fields' else
            }else{
                //Presents error to user
                let alert = UIAlertController(title: "Error!", message: "One of the fields is not filled out", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
            //Segment else
            //Checks to see if the login in segment is active
        }else if selectedOption.selectedSegmentIndex == 1{
            //Checks fields to make sure they aren't empty
            if emailText.text != "" || passwordText.text != ""{
                //Checks to see if there is a user with the information
                Auth.auth().signIn(withEmail: emailText.text!, password: passwordText.text!) { (user, error) in
                    if user != nil{
                        //Goes to the next view
                        self.performSegue(withIdentifier: "toMenu", sender: sender)
                    }
                    else{
                        //Presents user with error
                        if let myError = error?.localizedDescription{
                            let alert = UIAlertController(title: "Error!", message: myError, preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            
                            self.present(alert, animated: true)
                        }
                        else{
                            print("Error")
                        }
                    }
                }
            }
        }else{
            //Presents user with error
            let alert = UIAlertController(title: "Error!", message: "One of the fields is not filled out.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
        //End of func
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
