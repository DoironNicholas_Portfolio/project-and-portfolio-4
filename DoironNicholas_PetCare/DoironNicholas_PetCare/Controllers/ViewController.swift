//
//  ViewController.swift
//  DoironNicholas_PetCare
//
//  Created by Nicholas Doiron on 8/8/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    //Reference to database
    var ref: DatabaseReference!
    //Outlet
    @IBOutlet weak var navBar: UINavigationBar!
    //Selected button
    var selected = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //Sets the database's reference
        ref = Database.database().reference()
        //Gets the user's unique id
        let userID = Auth.auth().currentUser?.uid
        //Access the value of the user
        ref?.child("users").child(userID!).observeSingleEvent(of: .value) { (snapshot) in
            //Set up value to hold a value
            //let value = snapshot.value as? NSDictionary
            //Sets the title to be the user's email to show logged in
            self.navBar.topItem?.title = "Welcome"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func selectedView(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            selected = (sender.titleLabel?.text)!
        case 1:
            selected = (sender.titleLabel?.text)!
        case 2:
            selected = (sender.titleLabel?.text)!
        case 3:
            selected = (sender.titleLabel?.text)!
        default:
            selected = "error"
        }
        
        performSegue(withIdentifier: "toPetView", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! PetView
        destination.viewTitle = selected
    }
    
    @IBAction func logout(_ sender: UIBarButtonItem) {
        try! Auth.auth().signOut()
        dismiss(animated: true, completion: nil)
    }
}

