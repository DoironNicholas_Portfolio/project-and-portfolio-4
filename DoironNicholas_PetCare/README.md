# Pet Care

Installation instructions:
    Open the folder
    Click on the DoironNicholas_PetCare.xcworkspace
    Click the run button to run the app.
    
Hardware considerations and requirements
    The app was designed to work on the iPad and iPhone except for the iPhone 4 and iPhone SE.
    The app doesn't support landscape mode
    Currently some of the views won't scale correctly on the iPad
    
Any login requirements for testing
        The app should allow new and existing users to sign up or login
        
List of known bugs
    The text tries to change when the user clicks login
    Some of the view's text will change on its own.
    Not all information will display
    

